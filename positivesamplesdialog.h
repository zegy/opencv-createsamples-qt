#ifndef POSITIVESAMPLESDIALOG_H
#define POSITIVESAMPLESDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QDir>
#include <QSplitter>
#include "customgraphicsview.h"
#include <fstream>
#include <unistd.h>
#include <dirent.h>

namespace Ui {
class PositiveSamplesDialog;
}

class PositiveSamplesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PositiveSamplesDialog(QWidget *parent = 0, QDir dir = QDir(), QStringList imagesDirs = QStringList());
    ~PositiveSamplesDialog();

private slots:
    void on_listWidget_clicked(const QModelIndex &index);

    void on_buttonBox_rejected();

    void on_buttonBox_accepted();

private:
    Ui::PositiveSamplesDialog *ui;

    CustomGraphicsView *cgv;
    QGraphicsScene *scene;
    QPixmap image;
    QGraphicsPixmapItem *pixmap;
    QStringList imagesDirs;
    QDir dir;
};

#endif // POSITIVESAMPLESDIALOG_H
