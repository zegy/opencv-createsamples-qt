#-------------------------------------------------
#
# Project created by QtCreator 2016-08-28T18:47:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = opencv_createsamples-qt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    positivesamplesdialog.cpp \
    customgraphicsview.cpp

HEADERS  += mainwindow.h \
    positivesamplesdialog.h \
    customgraphicsview.h

FORMS    += mainwindow.ui \
    positivesamplesdialog.ui
