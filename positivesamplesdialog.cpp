#include "positivesamplesdialog.h"
#include "ui_positivesamplesdialog.h"

PositiveSamplesDialog::PositiveSamplesDialog(QWidget *parent, QDir dir, QStringList imagesDirs) :
    QDialog(parent),
    ui(new Ui::PositiveSamplesDialog)
{
    ui->setupUi(this);

    this->dir = dir;
    this->imagesDirs = imagesDirs;
    ui->listWidget->addItems(imagesDirs);
    scene = new QGraphicsScene();
    pixmap = scene->addPixmap(image);
    cgv = new CustomGraphicsView(*scene, imagesDirs);
    cgv->setScene(scene);
    cgv->setMouseTracking(true);
    //cgv->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    //cgv->setLayoutDirection(Qt::LeftToRight);
    QSplitter *splitter = new QSplitter();
    splitter->addWidget(cgv);
    splitter->addWidget(ui->listWidget);
    ui->horizontalLayout->addWidget(splitter/*cgv*/);
}

PositiveSamplesDialog::~PositiveSamplesDialog()
{
    delete ui;
}

void PositiveSamplesDialog::on_listWidget_clicked(const QModelIndex &index)
{
    // show image
    image.load(index.data().toString());
    pixmap->setPixmap(image);
    scene->setSceneRect(image.rect());
    cgv->setCurrentImage(index.data().toString());
    // debug show image name
    /*
    QMessageBox testMessage;
    testMessage.setText(index.data().toString());
    testMessage.exec();
    */
}

void PositiveSamplesDialog::on_buttonBox_rejected()
{
    QDialog::reject();
}

void PositiveSamplesDialog::on_buttonBox_accepted()
{
    std::ofstream info;
    QDir infoPath = dir; infoPath.cdUp();
    info.open((infoPath.absolutePath() + "/info").toStdString());

    std::pair<QString, CustomGraphicsView::verifyRect> obj;
    bool bSetRect;
    //int count = 1;
    foreach (obj, cgv->getImgObjs())
    {
        bSetRect = obj.second.second;
        if(bSetRect)
        {
            // write to file: image name number of objects x y width height
            info << (infoPath.relativeFilePath(obj.first) + " ").toStdString() << 1/*count*/ << " " << obj.second.first->rect().topLeft().    x() << " " << obj.second.first->rect().topLeft().    y() << " "\
                                                                    << obj.second.first->rect().bottomRight().x() << " " << obj.second.first->rect().bottomRight().y() << "\n";

            //count++;
        }
    }

    info.close();

    QMessageBox testMessage;
    testMessage.setText(tr(("\"Info\" file has been created in the following directory: " + infoPath.absolutePath()).toStdString().c_str()));
    testMessage.exec();

    QDialog::accept();
}
