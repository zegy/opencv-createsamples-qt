#include "customgraphicsview.h"

CustomGraphicsView::CustomGraphicsView(QGraphicsScene &scene, QStringList imagesDirs) :
    rectTopLeft(0, 0),
    rectBottomRight(0, 0)
{
    this->scene = &scene;
    rect.setTopLeft(rectTopLeft);
    rect.setBottomRight(rectBottomRight);

    rectObj = nullptr;

    QStringList::iterator imgdIt;
    for(imgdIt = imagesDirs.begin(); imgdIt != imagesDirs.end(); imgdIt++)
    {
        imgObj.insert(std::pair<QString, verifyRect>(*imgdIt, verifyRect(nullptr, false)));
    }

    bImageChanged = true;
    bNewObjRect = false;
}

CustomGraphicsView::~CustomGraphicsView()
{
    delete scene;
}

// set the image name of a clicked image for further manipulation
// and hide all object rectangles except the one of the current image
void CustomGraphicsView::setCurrentImage(QString imageName)
{
    curImageName = imageName;

    std::pair<QString, verifyRect> face;
    bool setRect;
    foreach (face, imgObj) {
        setRect = face.second.second;
        if(setRect)
        {
            if(face.first == imageName)
                face.second.first->show();
            else
                face.second.first->hide();
        }
    }

    bImageChanged = true;
}

CustomGraphicsView::imagesObjs CustomGraphicsView::getImgObjs()
{
    return imgObj;
}

void CustomGraphicsView::mousePressEvent(QMouseEvent *event)
{
    rectTopLeft = mapToScene(event->x(), event->y());
    if(sceneRect().contains(rectTopLeft))
    {
        rect.setTopLeft(rectTopLeft);
    }
}

void CustomGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event)

    // debug show images objs coords
    /*
    QMessageBox testMessage;
    std::pair<QString, verifyRect> face;
    foreach(face, imgf)
    {
        testMessage.setText(QString::number(face.second.first == nullptr?1337:face.second.first->rect().topLeft().x()) + "," +\
                            QString::number(face.second.first == nullptr?1337:face.second.first->rect().topLeft().y()) + ":" +\
                            QString::number(face.second.first == nullptr?1337:face.second.first->rect().bottomRight().x()) + "," +\
                            QString::number(face.second.first == nullptr?1337:face.second.first->rect().bottomRight().y()));
        testMessage.exec();
    }
    */
}

void CustomGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    // check if cursor is in the scene and lmb is pressed then set the new rectangle to draw
    rectBottomRight = mapToScene(event->x(), event->y());
    if(event->buttons() == Qt::LeftButton && sceneRect().contains(rectBottomRight))
    {
        rect.setBottomRight(rectBottomRight);
        bNewObjRect = true;
    }
}

void CustomGraphicsView::paintEvent(QPaintEvent *event)
{
    QGraphicsView::paintEvent(event);

    int imgSet = imgObj.count(curImageName);
    bool rectSet = imgObj.find(curImageName)->second.second;

    if(imgSet > 0)
    {
        rectObj = &imgObj.find(curImageName)->second;
        if(bImageChanged && !rectSet && bNewObjRect)
        {
            rectObj->first = scene->addRect(rect, QPen(QBrush(Qt::gray), 1, Qt::DashLine));
            rectObj->second = true;
            bImageChanged = false;
            bNewObjRect = false;
        }

        if(bNewObjRect)
        {
            rectObj->first->setRect(rect);
            bNewObjRect = false;
        }
        scene->update();
    }
}
