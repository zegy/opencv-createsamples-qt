#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// create positive samples file
void MainWindow::on_pushButton_Create_clicked()
{
    posSamplesDir = QFileDialog::getExistingDirectory(this, tr("Open Images Directory"), "/home/");
    if(!posSamplesDir.absolutePath().isEmpty())
    {
        QStringList imgFilter, imagesDirs;
        imgFilter << "*.jpg" << "*.jpeg" << "*.png" << "*.bmp";
        QFileInfoList imagesDirsInfo = posSamplesDir.entryInfoList(imgFilter, QDir::Files);

        foreach (QFileInfo imgInfo, imagesDirsInfo)
            imagesDirs.append(imgInfo.absoluteFilePath());

        // if there are images in the directory then write relative to pos samples dir images dirs
        if(!imagesDirsInfo.isEmpty())
        {
            PSDlg = new PositiveSamplesDialog(this, posSamplesDir, imagesDirs);
            PSDlg->exec();
        }
        else
        {
            QMessageBox testMessage;
            testMessage.setText(tr(((posSamplesDir.absolutePath()).toStdString() + " doesn't contain any images!\nPlease add some images or select a differnt directory.").c_str()));
            testMessage.exec();
        }
    }
}

// create negative samples file
void MainWindow::on_pushButton_Create_3_clicked()
{
    // get negative samples working directory and create a negative samples file
    negSamplesDir = QFileDialog::getExistingDirectory(this, tr("Open Images Directory"), "/home/");

    if(!negSamplesDir.absolutePath().isEmpty())
    {
        // get all images in a directory
        QStringList imgFilter, imagesDirs;
        imgFilter << "*.jpg" << "*.jpeg" << "*.png" << "*.bmp";
        QFileInfoList imagesDirsInfo = negSamplesDir.entryInfoList(imgFilter, QDir::Files);

        QDir bgPath = negSamplesDir; bgPath.cdUp();

        // if there are images in the directory then write relative to neg samples dir images dirs
        if(!imagesDirsInfo.isEmpty())
        {
            foreach (QFileInfo imgInfo, imagesDirsInfo)
                imagesDirs.append(imgInfo.absoluteFilePath());

            std::ofstream bg;
            bg.open((bgPath.absolutePath() + "/Background").toStdString());

            foreach (QString imageDir, imagesDirs)
                bg << bgPath.relativeFilePath(imageDir).toStdString().c_str() << "\n";

            bg.close();

            QMessageBox testMessage;
            testMessage.setText(tr(("\"Background\" file has been created in the following directory: " + bgPath.absolutePath()).toStdString().c_str()));
            testMessage.exec();
        }
        else
        {
            QMessageBox testMessage;
            testMessage.setText(tr(((negSamplesDir.absolutePath()).toStdString() + " doesn't contain any images!\nPlease add some images or select a differnt directory.").c_str()));
            testMessage.exec();
        }
    }
}
