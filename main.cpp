#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);                                                          

    QString styleSheetStr =
                    "QGroupBox {                            \
                        border: 1px solid gray;             \
                        border-radius: 6px;                 \
                        margin-top: 0.5em;                  \
                    }                                       \
                                                            \
                    QGroupBox::title {                      \
                        subcontrol-origin: margin;          \
                        left: 10px;                         \
                        padding: 0 3px 0 3px;               \
                    }";

    a.setStyleSheet(styleSheetStr);

    MainWindow w;
    w.show();

    return a.exec();
}
