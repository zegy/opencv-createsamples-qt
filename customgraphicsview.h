#ifndef CUSTOMGRAPHICSVIEW_H
#define CUSTOMGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QMessageBox>
#include <QMouseEvent>
#include <QScrollBar>
#include <QRectF>
#include <QGraphicsRectItem>
#include <map>

class CustomGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    typedef std::pair<QGraphicsRectItem*, bool> verifyRect;
    typedef std::map<QString, verifyRect> imagesObjs;

public:
    CustomGraphicsView(QGraphicsScene &scene, QStringList imagesDirs);
    ~CustomGraphicsView();

    void setCurrentImage(QString imageName);
    imagesObjs getImgObjs();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    QGraphicsScene *scene;
    QRectF rect;
    QPointF rectTopLeft, rectBottomRight;
    verifyRect *rectObj;
    imagesObjs imgObj;
    bool bImageChanged;
    bool bNewObjRect;
    QString curImageName;
};

#endif // CUSTOMGRAPHICSVIEW_H
