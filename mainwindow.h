#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <positivesamplesdialog.h>
#include <QGroupBox>
#include <QPushButton>
#include <QToolButton>
#include <QRadioButton>
#include <QMessageBox>
#include <QFileDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_Create_clicked();

    void on_pushButton_Create_3_clicked();

private:
    Ui::MainWindow *ui;

    PositiveSamplesDialog* PSDlg;
    //QStringList imagesDirs;
    QDir posSamplesDir;
    QDir negSamplesDir;
};

#endif // MAINWINDOW_H
